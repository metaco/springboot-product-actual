-- MySQL dump 10.13  Distrib 5.7.25, for macos10.14 (x86_64)
--
-- Host: localhost    Database: product_sell
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actual_permission`
--

DROP TABLE IF EXISTS `actual_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_permission` (
  `permission_id` bigint(20) NOT NULL,
  `permission_name` varchar(31) NOT NULL COMMENT '权限名称',
  `permission_value` int(11) NOT NULL COMMENT '权限位值',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_permission`
--

LOCK TABLES `actual_permission` WRITE;
/*!40000 ALTER TABLE `actual_permission` DISABLE KEYS */;
INSERT INTO `actual_permission` VALUES (1,'BASIC',2,'2019-10-07 01:23:08','2019-12-13 13:54:17',0),(100,'GOD',511,'2019-10-07 02:13:48','2019-12-13 13:54:17',0);
/*!40000 ALTER TABLE `actual_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actual_role`
--

DROP TABLE IF EXISTS `actual_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_role` (
  `role_id` bigint(20) NOT NULL,
  `role_name` varchar(31) NOT NULL COMMENT '角色名称',
  `role_value` int(11) NOT NULL COMMENT '角色位值',
  `disabled` tinyint(3) NOT NULL DEFAULT '0' COMMENT '角色禁用 0-未禁用， 1-禁用',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_role`
--

LOCK TABLES `actual_role` WRITE;
/*!40000 ALTER TABLE `actual_role` DISABLE KEYS */;
INSERT INTO `actual_role` VALUES (1,'default_user',2,0,'2019-10-07 01:22:22','2019-10-07 01:22:23',0),(100,'scott',255,0,'2019-10-07 02:12:39','2019-10-07 02:12:41',0);
/*!40000 ALTER TABLE `actual_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actual_role_permission`
--

DROP TABLE IF EXISTS `actual_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_role_permission` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色-权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_role_permission`
--

LOCK TABLES `actual_role_permission` WRITE;
/*!40000 ALTER TABLE `actual_role_permission` DISABLE KEYS */;
INSERT INTO `actual_role_permission` VALUES (1,1,1,'2019-10-07 01:23:39','2019-10-07 01:23:41',0),(2,100,100,'2019-10-07 02:14:30','2019-10-07 02:14:32',0);
/*!40000 ALTER TABLE `actual_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actual_user`
--

DROP TABLE IF EXISTS `actual_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_user` (
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(31) NOT NULL COMMENT '实际用户名',
  `user_passcode` varchar(63) NOT NULL COMMENT '实际用户密码',
  `user_email` varchar(31) NOT NULL COMMENT '实际用户邮箱',
  `user_locked` int(11) NOT NULL DEFAULT '0' COMMENT '未锁定0，锁定1',
  `credential_expired` int(11) NOT NULL DEFAULT '0' COMMENT '用户过期0-未过期，1-已经过期',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_user`
--

LOCK TABLES `actual_user` WRITE;
/*!40000 ALTER TABLE `actual_user` DISABLE KEYS */;
INSERT INTO `actual_user` VALUES (1,'你好','$2a$10$/wAcmpNiuJNMjCerwqKNeu8RIjqRwL8fN7drO5SLCQuXzAv/C5c.y','wangful@security.com',0,0,'2019-10-07 01:11:19','2019-10-07 11:02:10',0);
/*!40000 ALTER TABLE `actual_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actual_user_role`
--

DROP TABLE IF EXISTS `actual_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_user_role` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '过期时间',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户-角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_user_role`
--

LOCK TABLES `actual_user_role` WRITE;
/*!40000 ALTER TABLE `actual_user_role` DISABLE KEYS */;
INSERT INTO `actual_user_role` VALUES (1,1,1,'2020-10-07 02:05:10','2019-10-07 02:05:14','2019-10-07 02:05:15',0),(2,1,100,'2019-12-07 02:14:59','2019-10-07 02:15:06','2019-10-07 02:15:07',0);
/*!40000 ALTER TABLE `actual_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `detail_id` varchar(32) NOT NULL,
  `order_id` varchar(32) NOT NULL,
  `product_id` varchar(32) NOT NULL,
  `product_name` varchar(64) NOT NULL COMMENT '商品名称',
  `product_price` decimal(8,2) NOT NULL COMMENT '商品价格',
  `product_quantity` int(11) NOT NULL COMMENT '商品数量',
  `product_icon` varchar(512) DEFAULT NULL COMMENT '缩略图',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单详情';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES ('1574471349502101306','1574471348725104101','F000S13','ZendFrameworkCookbook',1234.11,3,'http://cc.hery.jpg','2019-11-23 01:09:09','2019-11-23 01:09:09',0),('1574471427951158864','1574471427531139528','F000S13','ZendFrameworkCookbook',1234.11,3,'http://cc.hery.jpg','2019-11-23 01:10:28','2019-11-23 01:10:28',0),('1574471517616173447','1574471517602112040','F000S12','Laravel Books-11',23.10,3,'http://cc.hery.jpg','2019-11-23 01:11:57','2019-11-23 01:11:57',0),('SUB-ORD2019071115','MASTER-ORD2019071114','F000S11','Laravel Books',93.10,2,'http://cc.hery.jpg','2019-07-11 03:58:10','2019-07-11 03:58:10',0),('SUB-ORD2019071116','MASTER-ORD2019071114','F000S12','JDBC_product',142.64,4,'http://cc.hery.jpg','2019-07-11 03:58:10','2019-08-04 05:03:33',0),('SUB-ORD2019071118','MASTER-ORD2019071117','F000S13','ZendFrameworkCookbook',1234.11,2,'http://cc.hery.jpg','2019-07-11 03:58:47','2019-07-11 03:58:47',0),('SUB-ORD2019071119','MASTER-ORD2019071117','F000S12','Laravel Books-11',23.10,4,'http://cc.hery.jpg','2019-07-11 03:58:47','2019-07-11 03:58:47',0),('SUB-ORD2019071121','MASTER-ORD2019071120','F000S11','Laravel Books',93.10,1,'http://cc.hery.jpg','2019-07-11 04:57:02','2019-07-11 04:57:02',0),('SUB-ORD2019071122','MASTER-ORD2019071120','F000S12','Laravel Books-11',23.10,2,'http://cc.hery.jpg','2019-07-11 04:57:02','2019-07-11 04:57:02',0);
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_master`
--

DROP TABLE IF EXISTS `order_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_master` (
  `order_id` varchar(32) NOT NULL,
  `buyer_name` varchar(32) NOT NULL COMMENT '买家名字',
  `buyer_phone` varchar(32) NOT NULL COMMENT '买家电话',
  `buyer_address` varchar(64) NOT NULL COMMENT '买家名字',
  `buyer_openid` varchar(64) NOT NULL COMMENT '买家微信OPENID',
  `amount` decimal(8,2) NOT NULL COMMENT '订单金额',
  `order_status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,  默认0新下单',
  `pay_status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付状态,  默认0未支付',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_master`
--

LOCK TABLES `order_master` WRITE;
/*!40000 ALTER TABLE `order_master` DISABLE KEYS */;
INSERT INTO `order_master` VALUES ('1574471427531139528','你好啊啊','18192800384','龙岗区','Vdb90t8w0820fg',3702.33,1,1,'2019-11-23 01:10:28','2019-11-23 01:10:28',0),('MASTER-ORD2019071114','詹高','181928003894','深圳市福田区科学馆','7vfkkfk2kk3ggbg',278.60,2,0,'2019-07-11 03:58:10','2019-07-11 07:54:34',0),('MASTER-ORD2019071117','詹高','181928003894','深圳市福田区科学馆','7vfkkfk2kk3ggbg',2560.62,2,0,'2019-07-11 03:58:47','2019-07-11 07:56:08',0),('MASTER-ORD2019071120','詹高','181928003894','深圳市福田区科学馆','7vfkkfk2kk3ggbg',139.30,0,0,'2019-07-11 04:57:02','2019-07-11 04:57:02',0);
/*!40000 ALTER TABLE `order_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` varchar(32) NOT NULL,
  `product_name` varchar(64) NOT NULL COMMENT '商品名称',
  `product_price` decimal(8,2) NOT NULL COMMENT '单价',
  `product_stock` int(11) NOT NULL COMMENT '库存',
  `product_description` varchar(64) DEFAULT NULL COMMENT '描述',
  `product_icon` varchar(512) DEFAULT NULL COMMENT '缩略图',
  `product_status` int(11) NOT NULL DEFAULT '0' COMMENT '商品0上架， 1下架',
  `category_type` bigint(20) NOT NULL COMMENT '类目编号',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES ('F000S11','Laravel Books',93.10,95,'Laravel Framework guides','http://cc.hery.jpg',0,3,'2019-07-08 13:29:21','2019-07-11 07:54:34',0),('F000S12','Laravel Books-11',23.10,2,'Laravels- Framework guides','http://cc.hery.jpg',0,3,'2019-07-08 13:46:10','2019-07-11 07:56:08',0),('F000S124','productName-Product',3045.20,1,NULL,'http://item-jd.com/items',0,0,'2019-09-30 04:59:37','2019-11-23 00:40:31',0),('F000S13','ZendFrameworkCookbook',1234.11,7,'Zend Gudies','http://cc.hery.jpg',0,10,'2019-07-09 02:12:08','2019-11-23 01:10:28',0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(64) NOT NULL COMMENT '类目名称',
  `category_type` bigint(20) NOT NULL COMMENT '类目编号',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `unique_category_type` (`category_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='类目表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,'co',1,'2019-07-08 05:52:59','2019-07-08 05:53:33',0),(2,'d',3,'2019-07-08 05:52:32','2019-07-08 05:52:32',0),(5,'d',5,'2019-07-08 04:46:31','2019-07-08 05:52:21',0),(6,'Unit Testing',9,'2019-07-08 06:35:40','2019-07-08 06:35:40',0),(8,'Unit Testing3',7,'2019-07-08 06:36:35','2019-07-08 06:36:35',0),(9,'ZendFramework',10,'2019-07-09 02:11:34','2019-07-09 02:11:34',0);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_log`
--

DROP TABLE IF EXISTS `task_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_log` (
  `task_id` varchar(32) NOT NULL,
  `task_content_id` int(11) NOT NULL,
  `task_content_name` varchar(32) NOT NULL,
  `task_request_status` int(11) NOT NULL,
  `task_request_message` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_log`
--

LOCK TABLES `task_log` WRITE;
/*!40000 ALTER TABLE `task_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-23 18:34:48
