package org.springboot.feature.kernel.entity.security;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("actual_user")
public class ActualUser {

    /**
     * user_id       bigint                               not null
     *         primary key,
     *     user_name     varchar(31)                          not null comment '实际用户名',
     *     user_passcode varchar(31)                          not null comment '实际用户密码',
     *     user_email    varchar(31)                          not null comment '实际用户邮箱',
     *     user_status   tinyint(3) default 0                 not null comment '用户状态,  默认0未激活，1已激活',
     *     role_id       bigint     default 1                 not null comment '角色ID  默认1默认用户角色',
     *     created_time  timestamp  default CURRENT_TIMESTAMP not null,
     *     updated_time  timestamp  default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
     *     logic_flag    int        default 0                 not null
     */
    @TableId
    private Long userId;

    private String userName;

    private String userEmail;

    private String userPasscode;

    private int userLocked;

    private int credentialExpired;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    private int logicFlag;
}
