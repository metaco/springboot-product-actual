package org.springboot.feature.kernel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductParam {

    @NotBlank(message = "产品名称不能为空")
    private String productName;

    @NotNull(message = "产品单价不能为空")
    private BigDecimal productPrice;

    @Min(value = 1, message = "产品库存不能小于0")
    private int productStock;

    @NotBlank(message = "产品描述不能为空")
    private String productDescription;

    private String productIcon;

    private int productStatus;

    private long categoryType;
}
