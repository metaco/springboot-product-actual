package org.springboot.feature.kernel.service.product.impl;

import org.springboot.feature.kernel.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getProduct(int categoryId);
}
