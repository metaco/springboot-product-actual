package org.springboot.feature.kernel.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springboot.feature.kernel.entity.security.ActualUser;

public interface ActualUserRepository extends BaseMapper<ActualUser> {

}
