package org.springboot.feature.kernel.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceLoggerAspect {

    private final static Logger log = LoggerFactory.getLogger(ServiceLoggerAspect.class);

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Pointcut("execution(public * org.springboot.feature.kernel.service..*.*(..))")
    public void subServicePackageMethod() {
    }

    @Around("subServicePackageMethod()")
    public Object handleServiceMethodLog(ProceedingJoinPoint joinPoint) throws Throwable {
        // service method duration :
        // beginTime mask
        long beginTimeSecond = System.currentTimeMillis();
        // logging content
        String packageClassName = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        StringBuilder stringBuilder = new StringBuilder();
        for (Object arg : joinPoint.getArgs()) {
            String argumentItem = objectMapper.writeValueAsString(arg);
            stringBuilder
                    .append(String.format("[%s]", argumentItem))
                    .append(",");
        }

        String loggingContent = String.format("%s:%s # %s",
                packageClassName, methodName, stringBuilder.toString());
        log.info(loggingContent);
        Object proceed = joinPoint.proceed();
        // endTime mask
        long endTimeSecond = System.currentTimeMillis();
        log.info(String.format("%s:%s # 耗时[%s]ms", packageClassName, methodName, (endTimeSecond - beginTimeSecond)));
        return proceed;
    }
}
