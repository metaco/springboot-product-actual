package org.springboot.feature.kernel.enums;

import lombok.Getter;

@Getter
public enum EnumUserLocked {

    LOCKED(1, true),
    UNLOCKED(0, false);

    private int value;

    private boolean propertyBoolean;

    EnumUserLocked(int value, boolean propertyBoolean) {
        this.value = value;
        this.propertyBoolean = propertyBoolean;
    }

    public boolean isPropertyBoolean() {
        return propertyBoolean;
    }

    public static boolean isNonLocked(int value) {
        for (EnumUserLocked enumUserLocked : values()) {
            if (enumUserLocked.value == value) {
                return !enumUserLocked.propertyBoolean;
            }
        } // 悲观情况
        return false;
    }
}
