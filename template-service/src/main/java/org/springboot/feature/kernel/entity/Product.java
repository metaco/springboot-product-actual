package org.springboot.feature.kernel.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@TableName("product")
public class Product {

    @TableId
    private String productId;

    private String productName;

    private BigDecimal productPrice;

    private int productStock;

    private String productDescription;

    private String productIcon;

    private int productStatus;

    private long categoryType;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
