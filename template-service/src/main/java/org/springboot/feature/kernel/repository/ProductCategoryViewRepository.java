package org.springboot.feature.kernel.repository;

import org.apache.ibatis.annotations.Param;
import org.springboot.feature.kernel.view.product.ProductCategoryView;

import java.util.List;

public interface ProductCategoryViewRepository {

    List<ProductCategoryView> findByCategoryId(@Param("categoryId") long categoryId);
}
