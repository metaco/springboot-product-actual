package org.springboot.feature.kernel.service.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class StringRedisCacheService {

    public final static String REDIS_CACHE_KEY_SEPARATOR = ":";

    private final StringRedisTemplate stringRedisTemplate;

    private ValueOperations<String, String> stringValueOperation;

    public StringRedisCacheService(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @PostConstruct
    public void init() {
        stringValueOperation = stringRedisTemplate.opsForValue();
        Assert.notNull(stringValueOperation, "opsForValue cloud not be null");
        log.info("StringValueOperation init..");
    }

    /**
     * setIfAbsent
     * @param key String
     * @param value String
     */
    public void setValue(String key, String value) {
        log.info("setValue key = {}, value = {}", key, value);
        if ( StringUtils.hasText(key) && StringUtils.hasText(value)) {
            stringValueOperation.setIfAbsent(key, value);
        }
    }

    public void setValue(String key, String value, long millSecond) {
        log.info("setValue key = {}, value = {}, millSecond = {}", key, value, millSecond);
        if (StringUtils.hasText(key) && StringUtils.hasText(value)) {
            stringValueOperation.setIfAbsent(key, value, millSecond,
                    TimeUnit.MILLISECONDS);
        }
    }

    public String getValue(String key) {
        return stringValueOperation.get(key);
    }

    public ValueOperations<String, String> getStringValueOperation() {
        return stringValueOperation;
    }
}
