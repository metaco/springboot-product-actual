package org.springboot.feature.kernel.enums;

import lombok.Getter;

@Getter
public enum EnumEntityState {

    NORMAL(0, "正常状态"),
    DELETED(1, "已删除");

    private int value;

    private String description;

    EnumEntityState(int value, String description) {
        this.value = value;
        this.description = description;
    }
}
