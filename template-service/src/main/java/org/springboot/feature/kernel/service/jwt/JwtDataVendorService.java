package org.springboot.feature.kernel.service.jwt;

import lombok.extern.slf4j.Slf4j;

import org.springboot.feature.core.properties.JWTSecurityProperties;
import org.springboot.feature.core.properties.SecurityProperties;
import org.springboot.feature.core.security.JsonWebTokenDataVendor;
import org.springboot.feature.core.security.TokenData;
import org.springboot.feature.kernel.service.cache.StringRedisCacheService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class JwtDataVendorService implements JsonWebTokenDataVendor {

    private final StringRedisCacheService stringCacheService;

    private final SecurityProperties securityProperties;

    private JWTSecurityProperties jwtSecurityProperties;

    public JwtDataVendorService(StringRedisCacheService stringCacheService,
                                SecurityProperties securityProperties) {
        Assert.notNull(securityProperties, "security properties was not empty");
        Assert.notNull(stringCacheService, "stringCacheService was not empty");
        this.stringCacheService = stringCacheService;
        this.securityProperties = securityProperties;
    }

    @PostConstruct
    public void init() {
        this.jwtSecurityProperties = securityProperties.getJwt();
    }

    public TokenData provide(String claimName) {
        log.info("provide claimName = {}", claimName);

        TokenData tokenData = new TokenData();
        String tokenCacheKey = jwtSecurityProperties.getTokenCacheKeyPrefix()
                + StringRedisCacheService.REDIS_CACHE_KEY_SEPARATOR
                + claimName;

        Object tokenValue = stringCacheService.getValue(tokenCacheKey);
        if (null != tokenValue) {
            tokenData.setToken(jwtSecurityProperties.getTokenPrefix() +  tokenValue);
            Long expireMinutes = stringCacheService.getStringValueOperation()
                    .getOperations()
                    .getExpire(tokenCacheKey, TimeUnit.SECONDS);
            Objects.requireNonNull(expireMinutes, "过期时间数据异常");
            tokenData.setExpiredIn(expireMinutes.intValue());
        }

        return tokenData;
    }
}
