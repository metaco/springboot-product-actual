package org.springboot.feature.kernel.repository;

import org.apache.ibatis.annotations.Param;
import org.springboot.feature.kernel.view.user.UserRolePermissionView;

import java.util.List;

public interface UserRolePermissionViewRepository {

    List<UserRolePermissionView> findByUserId(@Param("userId") long userId);
}
