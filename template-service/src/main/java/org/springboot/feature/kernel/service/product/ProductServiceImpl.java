package org.springboot.feature.kernel.service.product;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.kernel.dto.ProductDto;
import org.springboot.feature.kernel.entity.Product;
import org.springboot.feature.kernel.repository.ProductCategoryViewRepository;
import org.springboot.feature.kernel.repository.ProductRepository;
import org.springboot.feature.kernel.service.product.impl.ProductService;
import org.springboot.feature.kernel.view.product.ProductCategoryView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ProductServiceImpl extends ServiceImpl<ProductRepository, Product> implements ProductService {

    private final ProductCategoryViewRepository productCategoryViewRepository;

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductCategoryViewRepository productCategoryViewRepository,
                              ProductRepository productMapper) {
        this.productCategoryViewRepository = productCategoryViewRepository;
        this.productRepository = productMapper;
    }

    public List<ProductCategoryView> getCategoryProduct(long categoryId) {
        List<ProductCategoryView> productCategoryViews = productCategoryViewRepository.findByCategoryId(categoryId);
        log.info("productCategoryViews = {}", productCategoryViews.toString());
        // boolean stringOrderId = orderService.orderExist("stringOrderId");
        return  productCategoryViews;
    }

    public Product create(Product product) {
        LambdaQueryWrapper<Product> lambdaQueryWrapper = Wrappers.lambdaQuery();
        LambdaQueryWrapper<Product> queryWrapper = lambdaQueryWrapper
                .eq(Product::getProductName, product.getProductName())
                .and(i -> i.ne(Product::getUpdatedTime, LocalDateTime.now()));

        Product product1 = getOne(queryWrapper);
        if (null != product1) {
            return product1;
        }

        product1 = new Product();
        BeanUtils.copyProperties(product, product1);

        boolean saved = save(product1);
        if (! saved ) {
            return null;
        }
        return product1;
    }

    @Override
    public List<ProductDto> getProduct(int categoryId) {
        return null;
    }
}
