package org.springboot.feature.kernel.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {

    private String id;

    private String name;

    private BigDecimal Price;

    private int actualStock;

    private String description;
}
