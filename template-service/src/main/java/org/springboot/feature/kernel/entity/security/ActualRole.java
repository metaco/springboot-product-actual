package org.springboot.feature.kernel.entity.security;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("actual_user")
public class ActualRole {

    /**
     *
     *     role_name    varchar(31)                          not null comment '角色名称',
     *     role_value   int                                  not null comment '角色位值',
     *     disabled     tinyint(3) default 0                 not null comment '角色禁用 0-未禁用， 1-禁用',
     *     created_time  timestamp  default CURRENT_TIMESTAMP not null,
     *     updated_time  timestamp  default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
     *     logic_flag    int        default 0                 not null
     */
    @TableId
    private Long roleId;

    private String roleName;

    private int roleValue;

    private int disabled;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    private int logicFlag;
}
