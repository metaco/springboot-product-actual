package org.springboot.feature.kernel.view.product;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class ProductCategoryView {

    private String productId;

    private Long categoryId;

    private String categoryName;

    private String productName;

    private BigDecimal productPrice;

    @Override
    public String toString() {
        return "ProductCategoryView{" +
                "productId='" + productId + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                '}';
    }
}
