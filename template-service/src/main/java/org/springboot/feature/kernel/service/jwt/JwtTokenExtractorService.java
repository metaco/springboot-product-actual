package org.springboot.feature.kernel.service.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.security.JwtTokenCacheExtractor;
import org.springboot.feature.kernel.service.cache.StringRedisCacheService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JwtTokenExtractorService implements JwtTokenCacheExtractor {

    private final StringRedisCacheService stringRedisCacheService;

    public JwtTokenExtractorService(StringRedisCacheService stringRedisCacheService) {
        this.stringRedisCacheService = stringRedisCacheService;
    }

    @Override
    public String extract(String tokenCacheKey) {
        return stringRedisCacheService.getValue(tokenCacheKey);
    }

    @Override
    public void cache(String tokenCacheKey, String token, long durationMillSeconds) {
        stringRedisCacheService.setValue(tokenCacheKey, token, durationMillSeconds);
    }
}
