package org.springboot.feature.kernel.service.order;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class OrderService {

    public boolean orderExist(String orderId) {
        return StringUtils.hasText(orderId);
    }
}
