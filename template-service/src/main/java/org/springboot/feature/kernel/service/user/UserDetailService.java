package org.springboot.feature.kernel.service.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.kernel.entity.security.ActualUser;
import org.springboot.feature.kernel.enums.EnumEntityState;
import org.springboot.feature.kernel.repository.UserRolePermissionViewRepository;
import org.springboot.feature.kernel.view.user.UserRolePermissionView;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserDetailService implements UserDetailsService {

    private final ActualUserService actualUserService;

    private final UserRolePermissionViewRepository userRolePermissionViewRepository;

    public UserDetailService(ActualUserService actualUserService, UserRolePermissionViewRepository userRolePermissionViewRepository) {
        this.actualUserService = actualUserService;
        this.userRolePermissionViewRepository = userRolePermissionViewRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("loadUserByUserName(userEmail):{}",username);
        LambdaQueryWrapper<ActualUser> actualUserLambdaQueryWrapper = Wrappers.lambdaQuery();
        actualUserLambdaQueryWrapper
                .eq(ActualUser::getUserEmail, username)
                .eq(ActualUser::getLogicFlag, EnumEntityState.NORMAL.getValue());

        ActualUser actualUser = actualUserService.getOne(actualUserLambdaQueryWrapper);
        if (null == actualUser) {
            throw new UsernameNotFoundException(username);
        }

        List<UserRolePermissionView> rolePermissions = userRolePermissionViewRepository
                .findByUserId(actualUser.getUserId());

        return org.springframework.security.core.userdetails.User//
                .withUsername(actualUser.getUserEmail())//
                .password(actualUser.getUserPasscode())//
                .authorities(rolePermissions)//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)// 凭证暂不过期
                .disabled(false)//
                .build();
    }
}
