package org.springboot.feature.kernel.service.user;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.kernel.entity.security.ActualUser;

import org.springboot.feature.kernel.repository.ActualUserRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ActualUserService extends ServiceImpl<ActualUserRepository, ActualUser> {

    private final ActualUserRepository userRepository;

    public ActualUserService(ActualUserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
