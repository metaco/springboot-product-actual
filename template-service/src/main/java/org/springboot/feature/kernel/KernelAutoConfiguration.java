package org.springboot.feature.kernel;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class KernelAutoConfiguration {
}
