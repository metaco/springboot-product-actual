package org.springboot.feature.kernel.enums;

import lombok.Getter;

@Getter
public enum EnumUserExpired {

    EXPIRED(1, false),
    AVAILABLE(0, true);

    private int value;

    private boolean propertyBoolean;

    EnumUserExpired(int value, boolean propertyBoolean) {
        this.value = value;
        this.propertyBoolean = propertyBoolean;
    }

    public static boolean isNonExpired(int value) {
        for (EnumUserExpired enumUserLocked : values()) {
            if (enumUserLocked.value == value) {
                return !enumUserLocked.propertyBoolean;
            }
        }
        // 悲观情况
        return false;
    }
}
