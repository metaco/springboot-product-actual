package org.springboot.feature.kernel.view.user;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;

@Data
public class UserRolePermissionView implements GrantedAuthority {

    private Long userId;

    private Long roleId;

    private Long permissionId;

    private String roleName;

    private int roleValue;

    private String permissionName;

    private int permissionValue;

    private LocalDateTime expiredAt;

    @Override
    public String getAuthority() {
        return permissionName;
    }
}
