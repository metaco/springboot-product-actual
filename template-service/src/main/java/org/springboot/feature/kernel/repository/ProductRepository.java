package org.springboot.feature.kernel.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springboot.feature.kernel.entity.Product;

import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository extends BaseMapper<Product> {

    List<Product> findByProductStockAndPrice(
            @Param("productStock") int productStock, @Param("price")BigDecimal price);

    List<Product> findByCategoryType(@Param("categoryType") int categoryType);
}
