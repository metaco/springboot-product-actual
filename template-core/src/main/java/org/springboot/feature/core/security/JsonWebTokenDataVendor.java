package org.springboot.feature.core.security;

public interface JsonWebTokenDataVendor {

    TokenData provide(String username);
}
