package org.springboot.feature.core.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResult<T> {

    @JsonProperty(value = "error_code")
    private int errorCode;

    @JsonProperty(value = "error_message")
    private String errorMessage;

    private T data;

    public JsonResult(int errorCode, String errorMessage, T data) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public JsonResult(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public JsonResult() {

    }
}
