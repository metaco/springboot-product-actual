package org.springboot.feature.core.exception;

public class ActualProcessingException extends RuntimeException {

    private int serviceErrorCode;

    public ActualProcessingException(String message) {
        super(message);
    }

    public ActualProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActualProcessingException(String message, int serviceErrorCode) {

        super(String.format("服务处理异常 : %d # %s", serviceErrorCode, message));
    }
}
