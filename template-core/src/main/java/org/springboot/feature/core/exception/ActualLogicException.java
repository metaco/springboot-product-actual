package org.springboot.feature.core.exception;

public class ActualLogicException extends ActualProcessingException {
    public ActualLogicException(String message) {
        super(message);
    }

    public ActualLogicException(String message, int serviceErrorCode) {
        super(message, serviceErrorCode);
    }
}
