package org.springboot.feature.core.properties;


public class JWTSecurityProperties {
    // JWT 秘钥
    private String secret;
    // JWT  发行方
    private String issuer = "http://localhost";
    // 过期时间
    private long expirationSecond = 86400L;
    // token 前缀
    private String tokenPrefix = "Bearer ";

    private String headerString = "Authorization";

    private String claimName = "username";

    private String tokenCacheKeyPrefix = "actual-token";

    private String authLoginUrl = "/auth/login";

    public String getAuthLoginUrl() {
        return authLoginUrl;
    }

    public void setAuthLoginUrl(String authLoginUrl) {
        this.authLoginUrl = authLoginUrl;
    }

    public String getSecret() {
        return secret;
    }

    public String getTokenCacheKeyPrefix() {
        return tokenCacheKeyPrefix;
    }

    public void setTokenCacheKeyPrefix(String tokenCacheKeyPrefix) {
        this.tokenCacheKeyPrefix = tokenCacheKeyPrefix;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public long getExpirationSecond() {
        return expirationSecond;
    }

    public String getClaimName() {
        return claimName;
    }

    public void setClaimName(String claimName) {
        this.claimName = claimName;
    }

    public void setExpirationSecond(long expirationSecond) {
        this.expirationSecond = expirationSecond;
    }

    public String getTokenPrefix() {
        return tokenPrefix;
    }

    public void setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
    }

    public String getHeaderString() {
        return headerString;
    }

    public void setHeaderString(String headerString) {
        this.headerString = headerString;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
