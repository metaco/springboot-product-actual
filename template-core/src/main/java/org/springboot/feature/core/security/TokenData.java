package org.springboot.feature.core.security;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TokenData {

    private String token;

    @JsonProperty(value = "expired_in")
    private int expiredIn;

//    /**
//     * 0-未过期, -1 已过期, 大于 > 0 建议刷新Token
//     */
//    @JsonProperty(value = "expiration_type")
//    private int expirationType;
}
