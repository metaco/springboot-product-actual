package org.springboot.feature.core.security.filter;

import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.properties.SecurityProperties;
import org.springboot.feature.core.security.JwtTokenFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 过滤器处理所有HTTP请求，并检查是否存在带有正确令牌的Authorization标头。
 * 例如，如果令牌未过期或签名密钥正确。
   如果令牌有效，则过滤器会将身份验证数据添加到Spring的安全上下文中。
 */

@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final UserDetailsService userDetailsService;

    private final JwtTokenFactory jwtTokenFactory;

    private final SecurityProperties securityProperties;

    // 会从 Spring Security 配置文件那里传过来
    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  UserDetailsService userDetailsService,
                                  JwtTokenFactory jwtTokenFactory,
                                  SecurityProperties securityProperties) {
        super(authenticationManager);
        this.userDetailsService = userDetailsService;
        this.jwtTokenFactory = jwtTokenFactory;
        this.securityProperties = securityProperties;
    }


    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        // 判断是否有 token，并且进行认证
        Authentication authenticationToken = getAuthentication(request);
        // 如果请求头中没有token信息则直接放行了
        if (authenticationToken == null) {
            chain.doFilter(request, response);
            return ;
        }
        // 认证成功
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String header = request.getHeader(securityProperties.getJwt().getHeaderString());
        if (header == null || !header.startsWith(
                securityProperties.getJwt().getTokenPrefix())) {
            return null;
        }

        String token = header.replace(securityProperties.getJwt().getTokenPrefix(), "");
        String username = jwtTokenFactory.parse(token);
        UserDetails userDetails;
        try {
            userDetails = userDetailsService.loadUserByUsername(username);
        } catch (UsernameNotFoundException e) {
            log.error("getAuthentication error exception:", e);
            return null;
        }
        if (! jwtTokenFactory.verify(token, username)) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(userDetails.getUsername(),
                userDetails.getPassword(), userDetails.getAuthorities());
    }
}
