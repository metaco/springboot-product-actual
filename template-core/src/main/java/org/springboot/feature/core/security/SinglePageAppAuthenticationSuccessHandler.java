package org.springboot.feature.core.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当 Spring Security 默认使用 UserDetailsService 完成用户认证加载之后，
 * 会调用此接口中的 onAuthenticationSuccess
 */
@Slf4j
@Component("singlePageAppAuthenticationSuccessHandler")
public class SinglePageAppAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication)
            throws IOException, ServletException {
        log.info("自定义认证成功处理器#处理中...");

        String successContent = objectMapper.writeValueAsString(
                new JsonResult<>(0, "success", authentication));
        log.info("返回认证数据: successContent = {}", successContent);
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        httpServletResponse.getWriter()
                .write(successContent);
        log.info("自定义认证成功处理器#完成...");
    }
}
