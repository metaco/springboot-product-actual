package org.springboot.feature.core.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;


import org.springboot.feature.core.properties.SecurityProperties;
import org.springboot.feature.core.security.JsonWebTokenDataVendor;
import org.springboot.feature.core.security.JwtTokenFactory;
import org.springboot.feature.core.security.TokenData;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final static ObjectMapper mapper = new ObjectMapper();

    private final JwtTokenFactory jwtTokenFactory;

    private final JsonWebTokenDataVendor jwtDataVendorService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
                                   JwtTokenFactory jwtTokenFactory,
                                   SecurityProperties securityProperties,
                                   JsonWebTokenDataVendor jwtDataVendorService) {
        super();
        this.jwtTokenFactory = jwtTokenFactory;
        this.jwtDataVendorService = jwtDataVendorService;
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        // 从请求的 POST 中拿取 username 和 password 两个字段进行登入
        String username = request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
        String password = request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);
        if (StringUtils.hasText(username) && StringUtils.hasText(password)) {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            // 设置一些客户 IP 信息
            // setDetails(request, token);
            // 交给 AuthenticationManager 进行鉴权
            return getAuthenticationManager().authenticate(token);
        }
        logger.error("attempt username or password was empty");
        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        handleResponse(request, response, authResult, null);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        handleResponse(request, response, null, failed);
    }

    private void handleResponse(HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authResult,
                                AuthenticationException failed) throws IOException, ServletException {
        JsonResult<TokenData> jsonResult = new JsonResult<>();
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        if (authResult != null) {
            // 处理登入成功请求
            User user = (User) authResult.getPrincipal();
            String generate = jwtTokenFactory.generate(user.getUsername());
            TokenData tokenData = jwtDataVendorService.provide(user.getUsername());

            jsonResult.setErrorCode(0);
            jsonResult.setErrorMessage("登录成功");
            jsonResult.setData(tokenData);
            response.setStatus(HttpStatus.OK.value());
        } else {
            // 处理登入失败请求
            jsonResult.setErrorCode(-1001);
            jsonResult.setErrorMessage("用户名或密码错误");
            jsonResult.setData(null);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }

        response.getWriter().write(mapper.writeValueAsString(jsonResult));
    }
}
