package org.springboot.feature.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 *  Spring 装配类型：
 *      手动装配：模式注解 stereotype，相关注解派生
 *      模块装配：@Enable 注解派生
 *      条件装配：@Profile, @ConditionalOn
 *      自动装配：
 *          1.模式注解
 *          2.模块装配
 *          3. 工厂装配
 *  Spring Boot 自动装配
 */
@Configuration
@ComponentScan
public class TemplateCoreAutoConfiguration {
}
