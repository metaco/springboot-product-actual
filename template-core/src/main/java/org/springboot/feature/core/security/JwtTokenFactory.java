package org.springboot.feature.core.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.properties.JWTSecurityProperties;
import org.springboot.feature.core.properties.SecurityProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Component
public class JwtTokenFactory {

    private final SecurityProperties securityProperties;
    private JWTSecurityProperties jwtSecurityProperties;

    private final JwtTokenCacheExtractor jwtTokenCacheExtractor;

    private Algorithm algorithm;

    public JwtTokenFactory(SecurityProperties securityProperties, JwtTokenCacheExtractor jwtTokenCacheExtractor) {
        this.securityProperties = securityProperties;
        this.jwtTokenCacheExtractor = jwtTokenCacheExtractor;
    }

    @PostConstruct
    public void initAlgorithm() throws UnsupportedEncodingException {
        Objects.requireNonNull(securityProperties.getJwt(), "JWT configuration could not be null");
        jwtSecurityProperties = securityProperties.getJwt();
        algorithm = Algorithm.HMAC256(jwtSecurityProperties.getSecret());
        log.info("algorithm init..");
    }

    /**
     * 产生 JWT Token 对象
     * @param username String
     * @return String
     */
    public String generate(String username) {
        log.info("generate userDetails.userName = {}", username);
        String tokenKey = jwtSecurityProperties.getTokenCacheKeyPrefix() + ":" + username;

        String token = jwtTokenCacheExtractor.extract(tokenKey);

        if (null != token) {
            return token;
        }

        long expiredSecond = System.currentTimeMillis()
                + jwtSecurityProperties.getExpirationSecond() * 1000;
        Date expireDate = new Date(expiredSecond);

        String tokenString = JWT.create()
                .withClaim(jwtSecurityProperties.getClaimName(), username)
                .withExpiresAt(expireDate)
                .sign(algorithm);

        jwtTokenCacheExtractor.cache(tokenKey,
                tokenString,
                jwtSecurityProperties.getExpirationSecond() * 1000
        );
        return tokenString;
    }

    /**
     *  反解 token 至 User 对象上
     * @param token String
     * @return User
     */
    public String parse(String token) {
        log.info("parse token = {}", token);
        DecodedJWT decodedJWT = JWT.decode(token);
        String decodeUserDetail = decodedJWT.getClaim(jwtSecurityProperties.getClaimName()).asString();
        if (! StringUtils.hasText(decodeUserDetail)) {
            throw new IllegalStateException("jwt token parse empty string error");
        }
        return decodeUserDetail;
    }

    /**
     * 校验Token
     * @param token String
     * @param username String
     * @return boolean
     */
    public boolean verify(String token, String username) {
        log.info("verify token = {}, userDetails.username = {}", token, username);
        JWTVerifier verifier = JWT.require(algorithm)
                .withClaim(jwtSecurityProperties.getClaimName(), username)
                .build();
        try {

            DecodedJWT jwt = verifier.verify(token);
            Date expiresAt = jwt.getExpiresAt();
            long expiredSecond = expiresAt.toInstant().toEpochMilli();
            long remainMillSecond = expiredSecond - Instant.now().toEpochMilli();
            log.info("JWT token payloadInfo = {}, expiresAt = {}, remain = {} (ms)",
                    jwt.getClaims().toString(), expiresAt, remainMillSecond );
            return true;
        } catch (JWTVerificationException ex) {
            log.error("JWT Verification ex : {}", ex.getMessage());
            return false;
        }
    }

}
