package org.springboot.feature.core.advice;

import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.exception.ActualArgumentException;
import org.springboot.feature.core.exception.ActualLogicException;
import org.springboot.feature.core.exception.ActualProcessingException;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class RestProcessingExceptionHandler {

    @ExceptionHandler(value = ActualProcessingException.class)
    public JsonResult<String> processingServiceError(ActualProcessingException ex) {
        String errorMessage = "服务错误 服务名【%s】方法【%s】错误信息: %s";
        if (ex instanceof ActualArgumentException) {
            String message = String.format(errorMessage, "服务参数不合法", "processing", ex.getMessage());
            log.error(message);
            return new JsonResult<>(-10011, message);
        }

        if (ex instanceof ActualLogicException) {
            log.error("服务处理逻辑异常：", ex);
            return new JsonResult<>(-10012, ex.getMessage());
        }

        return new JsonResult<>(-10015, ex.getMessage());
    }
}
