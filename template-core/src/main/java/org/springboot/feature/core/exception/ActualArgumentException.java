package org.springboot.feature.core.exception;

public class ActualArgumentException extends ActualProcessingException {

    public ActualArgumentException(String message) {
        super(message);
    }

    public ActualArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActualArgumentException(String message, int serviceErrorCode) {
        super(message, serviceErrorCode);
    }
}
