package org.springboot.feature.core.advice;

import org.springboot.feature.core.web.JsonResult;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * JSR 303 Argument 参数校验
 */
@RestControllerAdvice
public class RestArgumentExceptionHandler {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public JsonResult<String> processingArgumentError(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();

        for (FieldError fieldError : fieldErrors) {
            if (StringUtils.hasLength(fieldError.getDefaultMessage())) {
                String errorMessage = String.format("【%s】%s",
                        fieldError.getField(), fieldError.getDefaultMessage());
                return new JsonResult<>(-10010, errorMessage);
            }
        }

        return new JsonResult<>(-10001, "Unknown error missing");
    }
}
