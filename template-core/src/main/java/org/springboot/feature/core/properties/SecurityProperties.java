package org.springboot.feature.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "actual.security")
public class SecurityProperties {

    private BrowserSecurityProperties browser = new BrowserSecurityProperties();

    private JWTSecurityProperties jwt = new JWTSecurityProperties();

    public BrowserSecurityProperties getBrowser() {
        return browser;
    }

    public void setBrowser(BrowserSecurityProperties browser) {
        this.browser = browser;
    }

    public JWTSecurityProperties getJwt() {
        return jwt;
    }

    public void setJwt(JWTSecurityProperties jwt) {
        this.jwt = jwt;
    }
}
