package org.springboot.feature.core.security;

public interface JwtTokenCacheExtractor {

    String extract(String tokenCacheKey);

    /**
     * 缓存 JWTToken 数据
     * @param tokenCacheKey String 键名
     * @param token String token
     * @param durationMillSeconds 过期剩余毫米数
     */
    void cache(String tokenCacheKey, String token, long durationMillSeconds);
}
