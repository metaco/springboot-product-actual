package org.springboot.feature.core.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 当 Spring Security 默认使用 UserDetailsService 完成用户认证失败之后，
 * 会调用此接口中的 onAuthenticationFailure
 */
@Slf4j
@Component("singlePageAppAuthenticationFailureHandler")
public class SinglePageAppAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("自定义认证失败处理器#处理中...");

        String successContent = objectMapper.writeValueAsString(
                new JsonResult<>(-1001, "登录认证失败", exception));
        log.info("返回认证失败数据: failureContent = {}", successContent);

        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        response.getWriter()
                .write(successContent);
        log.info("自定义认证失败处理器#完成.");
    }
}
