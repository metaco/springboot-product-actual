package org.springboot.feature.web.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


//*
// *  HTTP Request representation :
// *
//    scheme://host-address:server-port?queryString=value
//    httpMethod:
//    Content-Type:
//    Content-Length:
//    Content:
//    Cookie :
//    (etc..)
// *


@Slf4j
@Component
public class RestControllerLoggerInterceptor implements HandlerInterceptor {

    private final static String REQUEST_URL_LOGGING_FORMAT = "%s://%s:%s%s?%s";
    // private ContentCachingRequestWrapper cachingRequestWrapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long requestStartTime = System.currentTimeMillis();
        request.setAttribute("requestStartTime", requestStartTime);

        if (handler instanceof ResourceHttpRequestHandler) {
            return true;
        }

        String controllerName = ((HandlerMethod) handler).getBean().getClass().getName();
        String methodName = ((HandlerMethod) handler).getMethod().getName();
        log.info("======>>> request entry ======>>>");
        retrieveRequestUrl(request);
        retrieveBasicHeader(request);
        log.info(String.format("invoke controller [%s:%s]", controllerName, methodName));
        // cachingRequestWrapper = new ContentCachingRequestWrapper(request);

        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long requestEndTime = System.currentTimeMillis();
        Long requestStartTime = (Long) request.getAttribute("requestStartTime");

        if (handler instanceof ResourceHttpRequestHandler) {
            log.info("postHandle resourceHttpRequest..");
        } else {
            String controllerName = ((HandlerMethod) handler).getBean()
                    .getClass()
                    .getName();
            String methodName = ((HandlerMethod) handler).getMethod().getName();
            log.info(String.format("%s:%s 请求结束. 耗时[%s]ms ", controllerName, methodName,
                    (requestEndTime - requestStartTime)));
            log.info("<<<====== request exit <<<=======");
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (null != ex) {
            if (handler instanceof ResourceHttpRequestHandler) {
                log.info("resourceHttpRequestHandler");
            } else {
                long requestEndTime = System.currentTimeMillis();
                Long requestStartTime = (Long) request.getAttribute("requestStartTime");
                String controllerName = ((HandlerMethod) handler).getBean().getClass().getName();
                String methodName = ((HandlerMethod) handler).getMethod().getName();

                log.error(String.format("%s:%s 遭遇异常, 耗时[%s]ms , %s", controllerName, methodName,
                        (requestEndTime - requestStartTime), ex));
                log.info("<<<====== request exit <<<========");
            }
        }
    }


    private void retrieveBasicHeader(HttpServletRequest request) throws IOException {
        String requestMethod = request.getMethod();
        log.info("request method: [{}]", requestMethod);

        HttpMethod httpMethod = HttpMethod.resolve(requestMethod);
        if(HttpMethod.POST == httpMethod || HttpMethod.PUT == httpMethod) {
            log.info("request Content-Type: {}", request.getContentType());
            log.info("request Content-Length: {}", request.getContentLength());
        }
    }

    private void retrieveRequestUrl(HttpServletRequest request) {
        String scheme = request.getScheme();
        String localAddress = request.getLocalAddr();
        int serverPort = request.getServerPort();
        String requestURI = request.getRequestURI();
        String queryString = request.getQueryString();

        String format = "request url: " + REQUEST_URL_LOGGING_FORMAT;
        if (StringUtils.hasText(queryString)) {
            log.info(String.format(format, scheme, localAddress, serverPort, requestURI, queryString));
        } else {
            String content = String.format(format, scheme, localAddress, serverPort, requestURI, "");
            log.info(content.substring(0, content.length() - 1));
        }
    }
}
