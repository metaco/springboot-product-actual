package org.springboot.feature.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.web.JsonResult;
import org.springboot.feature.kernel.entity.Product;
import org.springboot.feature.kernel.model.ProductParam;
import org.springboot.feature.kernel.service.product.ProductServiceImpl;
import org.springboot.feature.kernel.view.product.ProductCategoryView;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductServiceImpl productServiceImpl;

    private final StringRedisTemplate stringRedisTemplate;


    public ProductController(ProductServiceImpl productServiceImpl, StringRedisTemplate stringRedisTemplate) {
        this.productServiceImpl = productServiceImpl;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @GetMapping("/category/view")
    public JsonResult<List<ProductCategoryView>> getCategoryView() {
        List<ProductCategoryView> categoryProduct = productServiceImpl.getCategoryProduct(2L);
        ValueOperations<String, String> stringValueOperations = stringRedisTemplate.opsForValue();
        stringValueOperations.set("key-name", "token-value", 2L, TimeUnit.MINUTES);
        return new JsonResult<>(0, "success", categoryProduct);
    }

    @PostMapping("")
    public JsonResult<Product> create(@Valid @RequestBody ProductParam productParam) {
        Product product = new Product();
        BeanUtils.copyProperties(productParam, product);
        Product product1 = productServiceImpl.create(product);
        if (null == product1) {
            return new JsonResult<>(-11, "创建product失败", product);
        }
        String query = "SELECT " +
                       "    t.* " +
                       "FROM " +
                       "    t_pms_logic_parking t " +
                       "WHERE " +
                       "    t.cn = 1 " +
                       "    AND t.logic_flag = 1 " +
                       " ";
        return new JsonResult<>(0, "success", product1);
    }
}
