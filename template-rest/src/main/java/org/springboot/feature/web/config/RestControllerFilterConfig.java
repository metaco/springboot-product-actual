package org.springboot.feature.web.config;


import org.springboot.feature.web.interceptor.RestControllerLoggerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RestControllerFilterConfig implements WebMvcConfigurer {

    private final RestControllerLoggerInterceptor restControllerLoggerInterceptor;

    public RestControllerFilterConfig(RestControllerLoggerInterceptor restControllerLoggerInterceptor) {
        this.restControllerLoggerInterceptor = restControllerLoggerInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(restControllerLoggerInterceptor);
    }
}
