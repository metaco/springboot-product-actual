package org.springboot.feature.web.config;

import org.springboot.feature.core.properties.SecurityProperties;
import org.springboot.feature.core.security.JsonWebTokenDataVendor;
import org.springboot.feature.core.security.JwtTokenFactory;
import org.springboot.feature.core.security.filter.JWTAuthenticationFilter;
import org.springboot.feature.core.security.filter.JwtAuthorizationFilter;
import org.springboot.feature.kernel.service.user.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // private final SinglePageAppAuthenticationSuccessHandler singlePageAppAuthenticationSuccessHandler;

    // private final SinglePageAppAuthenticationFailureHandler singlePageAppAuthenticationFailureHandler;

    private final UserDetailsService userDetailService;

    private final JwtTokenFactory jwtTokenFactory;

    private final SecurityProperties securityProperties;

    private final JsonWebTokenDataVendor jsonWebTokenDataVendor;

    @Autowired
    public WebSecurityConfig(
            UserDetailService userDetailService,
            JwtTokenFactory jwtTokenFactory,
            SecurityProperties securityProperties,
            JsonWebTokenDataVendor jsonWebTokenDataVendor) {

        this.userDetailService = userDetailService;
        this.jwtTokenFactory = jwtTokenFactory;
        this.securityProperties = securityProperties;
        this.jsonWebTokenDataVendor = jsonWebTokenDataVendor;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
            .and()
            .csrf().disable()
            .authorizeRequests()
                // 不是用 JWT Token 请求的资源地址
            .antMatchers(HttpMethod.POST,
                    securityProperties.getJwt().getAuthLoginUrl()).permitAll()
            // 指定路径下的资源需要验证的用户才能访问
            .antMatchers("/**").authenticated()
            .anyRequest().permitAll()
            .and()
            .addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtTokenFactory, securityProperties, jsonWebTokenDataVendor))
            .addFilter(new JwtAuthorizationFilter(authenticationManager(), userDetailService, jwtTokenFactory, securityProperties))
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }
}
