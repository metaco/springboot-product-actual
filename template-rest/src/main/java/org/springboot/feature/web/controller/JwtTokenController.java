package org.springboot.feature.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springboot.feature.core.security.JwtTokenFactory;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// @Slf4j
@RestController
@RequestMapping("/token")
public class JwtTokenController {

    private final JwtTokenFactory jwtTokenFactory;

    public JwtTokenController(JwtTokenFactory jwtTokenFactory) {
        this.jwtTokenFactory = jwtTokenFactory;
    }

    @GetMapping("/factory")
    public JsonResult<String> getToken(@RequestParam("username") String username) {
        String generationToken = jwtTokenFactory.generate(username);

        return new JsonResult<>(0, "success", generationToken);
    }

    @GetMapping("/decryption")
    public JsonResult<String> parseToken(@RequestParam("token") String token) {
        if (! StringUtils.hasText(token)) {
            return new JsonResult<>(-11, "token parameters was empty");
        }

        return new JsonResult<>(0, "success", jwtTokenFactory.parse(token));
    }
}
