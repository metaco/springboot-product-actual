package org.springboot.feature.web.config;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("org.springboot.feature.kernel.repository")
public class MybatisPlusConfig {
}
