package org.springboot.feature.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springboot.feature.core.web.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AuthController {

    @PostMapping("/auth/login")
    public JsonResult<String> getAuth() {
        return new JsonResult<>(0, "success", "auth");
    }
}
