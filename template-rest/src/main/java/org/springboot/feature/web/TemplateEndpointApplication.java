package org.springboot.feature.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateEndpointApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateEndpointApplication.class, args);
    }
}
