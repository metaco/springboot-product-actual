package org.springboot.feature.web;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springboot.feature.kernel.service.product.ProductServiceImpl;
import org.springboot.feature.kernel.view.product.ProductCategoryView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductServiceImplTest {

    @Autowired
    private ProductServiceImpl productServiceImpl;

    @Test
    public void testGetProduct() {
        List<ProductCategoryView> categoryProduct = productServiceImpl.getCategoryProduct(1L);
        Assert.assertEquals(0, categoryProduct.size());
    }
}
